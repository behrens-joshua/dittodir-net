﻿namespace DittoDir.GUI
{
    partial class UCOverview
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.btnSelectSource = new System.Windows.Forms.Button();
            this.tbSource = new System.Windows.Forms.TextBox();
            this.gbDestination = new System.Windows.Forms.GroupBox();
            this.btnSelectDestination = new System.Windows.Forms.Button();
            this.tbDestination = new System.Windows.Forms.TextBox();
            this.pbIdle = new System.Windows.Forms.ProgressBar();
            this.dlgFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.gbSource.SuspendLayout();
            this.gbDestination.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSource
            // 
            this.gbSource.Controls.Add(this.btnSelectSource);
            this.gbSource.Controls.Add(this.tbSource);
            this.gbSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbSource.Location = new System.Drawing.Point(0, 0);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(160, 46);
            this.gbSource.TabIndex = 0;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source:";
            // 
            // btnSelectSource
            // 
            this.btnSelectSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSelectSource.Location = new System.Drawing.Point(79, 17);
            this.btnSelectSource.Name = "btnSelectSource";
            this.btnSelectSource.Size = new System.Drawing.Size(75, 23);
            this.btnSelectSource.TabIndex = 1;
            this.btnSelectSource.Text = "Select";
            this.btnSelectSource.UseVisualStyleBackColor = true;
            this.btnSelectSource.Click += new System.EventHandler(this.btnSelectSource_Click);
            // 
            // tbSource
            // 
            this.tbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSource.Location = new System.Drawing.Point(6, 19);
            this.tbSource.Name = "tbSource";
            this.tbSource.Size = new System.Drawing.Size(67, 20);
            this.tbSource.TabIndex = 0;
            // 
            // gbDestination
            // 
            this.gbDestination.Controls.Add(this.btnSelectDestination);
            this.gbDestination.Controls.Add(this.tbDestination);
            this.gbDestination.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbDestination.Location = new System.Drawing.Point(0, 46);
            this.gbDestination.Name = "gbDestination";
            this.gbDestination.Size = new System.Drawing.Size(160, 46);
            this.gbDestination.TabIndex = 1;
            this.gbDestination.TabStop = false;
            this.gbDestination.Text = "Destination:";
            // 
            // btnSelectDestination
            // 
            this.btnSelectDestination.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSelectDestination.Location = new System.Drawing.Point(79, 17);
            this.btnSelectDestination.Name = "btnSelectDestination";
            this.btnSelectDestination.Size = new System.Drawing.Size(75, 23);
            this.btnSelectDestination.TabIndex = 1;
            this.btnSelectDestination.Text = "Select";
            this.btnSelectDestination.UseVisualStyleBackColor = true;
            this.btnSelectDestination.Click += new System.EventHandler(this.btnSelectDestination_Click);
            // 
            // tbDestination
            // 
            this.tbDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDestination.Location = new System.Drawing.Point(6, 19);
            this.tbDestination.Name = "tbDestination";
            this.tbDestination.Size = new System.Drawing.Size(67, 20);
            this.tbDestination.TabIndex = 0;
            // 
            // pbIdle
            // 
            this.pbIdle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbIdle.Location = new System.Drawing.Point(0, 92);
            this.pbIdle.Name = "pbIdle";
            this.pbIdle.Size = new System.Drawing.Size(160, 23);
            this.pbIdle.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbIdle.TabIndex = 2;
            // 
            // UCOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pbIdle);
            this.Controls.Add(this.gbDestination);
            this.Controls.Add(this.gbSource);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(160, 115);
            this.Name = "UCOverview";
            this.Size = new System.Drawing.Size(160, 115);
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbDestination.ResumeLayout(false);
            this.gbDestination.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.Button btnSelectSource;
        private System.Windows.Forms.TextBox tbSource;
        private System.Windows.Forms.GroupBox gbDestination;
        private System.Windows.Forms.Button btnSelectDestination;
        private System.Windows.Forms.TextBox tbDestination;
        private System.Windows.Forms.ProgressBar pbIdle;
        private System.Windows.Forms.FolderBrowserDialog dlgFolder;
    }
}
