﻿namespace DittoDir.GUI
{
    /// <summary>
    /// The main form of the application.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class FrmOverview : System.Windows.Forms.Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmOverview"/> class.
        /// </summary>
        public FrmOverview()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the DoWork event of the bgwDitto control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void bgwDitto_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            DL.Ditto ditto = new DL.Ditto();
            ditto.Clone(e.Argument as DL.StartupParameter);
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the bgwDitto control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void bgwDitto_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            ucOverview1.IsRunning = false;
            btnStart.Enabled = true;
        }

        /// <summary>
        /// Handles the Click event of the btnStart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnStart_Click(object sender, System.EventArgs e)
        {
            btnStart.Enabled = false;
            ucOverview1.IsRunning = true;
            bgwDitto.RunWorkerAsync(new DL.StartupParameter()
            {
                Destination = ucOverview1.Destination,
                DestinationType = DL.Destination.DestinationType.Directory,
                Source = ucOverview1.Source,
                SourceType = DL.Source.SourceType.Directory
            });
        }
    }
}
