﻿namespace DittoDir.GUI
{
    /// <summary>
    /// UserControl containing the overview input.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class UCOverview : System.Windows.Forms.UserControl
    {
        /// <summary>
        /// Indicating whether this instance is running.
        /// </summary>
        private bool _isRunning = false;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is running.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }

            set
            {
                _isRunning = value;
                pbIdle.Visible = value;

                btnSelectSource.Enabled = !value;
                tbSource.Enabled = !value;
                btnSelectDestination.Enabled = !value;
                tbDestination.Enabled = !value;
            }
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        public string Source
        {
            get
            {
                return tbSource.Text;
            }

            set
            {
                tbSource.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        public string Destination
        {
            get
            {
                return tbDestination.Text;
            }

            set
            {
                tbDestination.Text = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UCOverview"/> class.
        /// </summary>
        public UCOverview()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the btnSelectSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnSelectSource_Click(object sender, System.EventArgs e)
        {
            dlgFolder.ShowNewFolderButton = false;

            if (dlgFolder.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                Source = dlgFolder.SelectedPath;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSelectDestination control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void btnSelectDestination_Click(object sender, System.EventArgs e)
        {
            dlgFolder.ShowNewFolderButton = true;

            if (dlgFolder.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                Destination = dlgFolder.SelectedPath;
            }
        }
    }
}
