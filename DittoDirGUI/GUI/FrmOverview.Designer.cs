﻿namespace DittoDir.GUI
{
    partial class FrmOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.ucOverview1 = new DittoDir.GUI.UCOverview();
            this.bgwDitto = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnStart.Location = new System.Drawing.Point(92, 128);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // ucOverview1
            // 
            this.ucOverview1.Destination = "";
            this.ucOverview1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucOverview1.IsRunning = false;
            this.ucOverview1.Location = new System.Drawing.Point(0, 0);
            this.ucOverview1.Margin = new System.Windows.Forms.Padding(0);
            this.ucOverview1.MinimumSize = new System.Drawing.Size(160, 115);
            this.ucOverview1.Name = "ucOverview1";
            this.ucOverview1.Size = new System.Drawing.Size(280, 120);
            this.ucOverview1.Source = "";
            this.ucOverview1.TabIndex = 0;
            // 
            // bgwDitto
            // 
            this.bgwDitto.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwDitto_DoWork);
            this.bgwDitto.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwDitto_RunWorkerCompleted);
            // 
            // FrmOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 163);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.ucOverview1);
            this.MinimumSize = new System.Drawing.Size(170, 190);
            this.Name = "FrmOverview";
            this.Text = "DittoDir";
            this.ResumeLayout(false);

        }

        #endregion

        private UCOverview ucOverview1;
        private System.Windows.Forms.Button btnStart;
        private System.ComponentModel.BackgroundWorker bgwDitto;
    }
}