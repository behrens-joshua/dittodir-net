﻿namespace DittoDir.GUI
{
    /// <summary>
    /// Class containing the entrypoint of the application.
    /// </summary>
    class Entrypoint
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        [System.STAThread()]
        public static void Main()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run(new FrmOverview());
        }
    }
}
