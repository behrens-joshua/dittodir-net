﻿namespace DittoDir.CLI
{
    /// <summary>
    /// The class containing the CLI entrypoint.
    /// </summary>
    class Entrypoint
    {
        /// <summary>
        /// Entrypoint of the CLI.
        /// </summary>
        /// <param name="arguments">The arguments passed to the CLI.</param>
        public static void Main(string[] arguments)
        {
        }
    }
}
