﻿using System;

namespace DittoDir.DL
{
    /// <summary>
    /// This class holds all needed information to run a cloning instruction.
    /// </summary>
    public class StartupParameter
    {
        /// <summary>
        /// Gets or sets the strategy to interpret the given Source.
        /// </summary>
        public DittoDir.DL.Source.SourceType SourceType
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets the Source.
        /// </summary>
        public string Source
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets the strategy to interpret the given Destination.
        /// </summary>
        public Destination.DestinationType DestinationType
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets the Destination
        /// </summary>
        public string Destination
        {
            get;

            set;
        }
    }
}
