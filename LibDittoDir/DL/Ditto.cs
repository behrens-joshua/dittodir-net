﻿using DittoDir.DL.Destination;
using DittoDir.DL.Source;
using DittoDir.DL.Transfer;
using System;

namespace DittoDir.DL
{
    /// <summary>
    /// Performs the main routine of cloning a source to a destination
    /// </summary>
    public class Ditto
    {

        /// <summary>
        /// This event is fired when a file or directory was successfully cloned
        /// </summary>
        public event EventHandler<ClonedEventArgs> ClonedEvent;

        /// <summary>
        /// Gets the used StartupParameter.
        /// </summary>
        public StartupParameter Parameter
        {
            get;

            private set;
        }

        /// <summary>
        /// Clones the specified source to the specified destination.
        /// </summary>
        /// <param name="parameter">The StartupParameter to use.</param>
        public void Clone(StartupParameter parameter)
        {
            Parameter = parameter;

            using (ISourceStrategy srcStrat = SourceHelper.GetStrategyByType(parameter.SourceType))
            {
                srcStrat.Source = parameter.Source;

                using (IDestinationStrategy destStrat = DestinationHelper.GetStrategyByType(parameter.DestinationType))
                {
                    destStrat.Destination = parameter.Destination;

                    foreach (MetaInformation meta in srcStrat.GenerateItems())
                    {
                        destStrat.ProcessItem(meta);
                        OnClonedEvent(meta);
                    }
                }
            }
        }

        /// <summary>
        /// Called when a file or directory was successfully cloned.
        /// The necessary are gathered from the meta information
        /// </summary>
        /// <param name="meta">The metadata.</param>
        protected void OnClonedEvent(MetaInformation meta)
        {
            OnClonedEvent(meta.SourceItem, meta.DestinationItem, meta.IsFile);
        }

        /// <summary>
        /// Called when a file or directory was successfully cloned.
        /// </summary>
        /// <param name="sourceItem">The source item.</param>
        /// <param name="destinationItem">The destination item.</param>
        /// <param name="isFile">if set to <c>true</c> [is file].</param>
        protected void OnClonedEvent(string sourceItem, string destinationItem, bool isFile)
        {
            EventHandler<ClonedEventArgs> handler = ClonedEvent;
            if (handler != null)
            {
                ClonedEventArgs args = new ClonedEventArgs(sourceItem, destinationItem, isFile);
                handler(this, args);
            }
        }
    }
}
