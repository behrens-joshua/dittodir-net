﻿using System;

namespace DittoDir.DL.Transfer
{
    /// <summary>
    /// Holds the meta information to transfer
    /// </summary>
    public struct MetaInformation
    {
        /// <summary>
        /// Gets or sets the timestamp when the item was created.
        /// </summary>
        public DateTime Created
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets the timestamp when the item was last modified.
        /// </summary>
        public DateTime LastModified
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets the size of the item in bytes.
        /// </summary>
        public long Size
        {
            get;

            set;
        }

        /// <summary>
        /// Indicates, whether the item is a file or a directory.
        /// </summary>
        public bool IsFile
        {
            get;

            private set;
        }

        /// <summary>
        /// Gets the source specific item.
        /// </summary>
        public string SourceItem
        {
            get;

            private set;
        }

        /// <summary>
        /// Gets or sets the transfer specific item. It has to be clean from any source or destination related information.
        /// </summary>
        public string TransferItem
        {
            get;

            set;
        }

        /// <summary>
        /// Gets or sets the destination specific item.
        /// </summary>
        public string DestinationItem
        {
            get;

            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MetaInformation"/> struct.
        /// </summary>
        /// <param name="isFile">Indicator, whether the item is a file or a directory.</param>
        /// <param name="sourceItem">The source item.</param>
        public MetaInformation(bool isFile, string sourceItem)
            : this()
        {
            IsFile = isFile;
            SourceItem = sourceItem;
        }
    }
}
