﻿namespace DittoDir.DL.Exceptions
{
    /// <summary>
    /// This exception is thrown, when a requested source strategy is not implemented yet.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class SourceStrategyNotImplementedException : System.Exception
    {
        /// <summary>
        /// Gets the type of the requested source strategy.
        /// </summary>
        public Source.SourceType SourceType
        {
            get;

            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceStrategyNotImplementedException"/> class.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        public SourceStrategyNotImplementedException(Source.SourceType sourceType)
        {
            SourceType = sourceType;
        }
    }
}
