﻿namespace DittoDir.DL.Exceptions
{
    /// <summary>
    /// This exception is thrown, when a requested destination strategy is not implemented yet.
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class DestinationStrategyNotImplementedException : System.Exception
    {
        /// <summary>
        /// Gets the type of the requested destination strategy.
        /// </summary>
        public Destination.DestinationType DestinationType
        {
            get;

            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DestinationStrategyNotImplementedException"/> class.
        /// </summary>
        /// <param name="destinationType">Type of the destination.</param>
        public DestinationStrategyNotImplementedException(Destination.DestinationType destinationType)
        {
            DestinationType = destinationType;
        }
    }
}
