﻿namespace DittoDir.DL.Source
{
    /// <summary>
    /// Helping class to simplify general interactions with any source specific class.
    /// </summary>
    internal class SourceHelper
    {
        /// <summary>
        /// Gets the strategy by the given type.
        /// </summary>
        /// <param name="sourceType">Type of the source strategy.</param>
        /// <returns>
        /// The corresponding strategy to the given sourceType.
        /// </returns>
        /// <exception cref="DittoDir.DL.Exceptions.SourceStrategyNotImplementedException"></exception>
        public static ISourceStrategy GetStrategyByType(SourceType sourceType)
        {
            switch (sourceType)
            {
                case SourceType.Directory:
                    return new Strategies.DirectoryStrategy();
            }

            throw new Exceptions.SourceStrategyNotImplementedException(sourceType);
        }
    }
}
