﻿using System;
using System.IO;
using System.Collections.Generic;

namespace DittoDir.DL.Source.Strategies
{
    /// <summary>
    /// Interprets the given source as directory. Corresponding strategy to SourceType.Directory.
    /// </summary>
    /// <seealso cref="DittoDir.DL.Source.ISourceStrategy" />
    internal class DirectoryStrategy : ISourceStrategy
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        public string Source
        {
            get;

            set;
        }

        /// <summary>
        /// Generates all MetaInformation to copy.
        /// </summary>
        /// <returns>
        /// An enumerator on the requests MetaInformation.
        /// </returns>
        public IEnumerable<Transfer.MetaInformation> GenerateItems()
        {
            if (Source == null)
            {
                throw new ArgumentNullException("this.Source");
            }

            Queue<DirectoryInfo> todoList = new Queue<DirectoryInfo>();
            todoList.Enqueue(new DirectoryInfo(Source));

            while (todoList.Count > 0)
            {
                DirectoryInfo currentDir = todoList.Dequeue();

                if (currentDir.Exists)
                {
                    yield return new Transfer.MetaInformation(false, currentDir.FullName)
                    {
                        Created = currentDir.CreationTime,
                        LastModified = currentDir.LastWriteTime,
                        Size = 0,
                        TransferItem = GetRelativePath(currentDir.FullName, Source)
                    };

                    foreach (DirectoryInfo subDir in currentDir.GetDirectories())
                    {
                        todoList.Enqueue(subDir);
                    }

                    foreach (FileInfo fileInfo in currentDir.GetFiles())
                    {
                        yield return new Transfer.MetaInformation(true, fileInfo.FullName)
                        {
                            Created = currentDir.CreationTime,
                            LastModified = currentDir.LastWriteTime,
                            Size = fileInfo.Length,
                            TransferItem = GetRelativePath(Source.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar, fileInfo.FullName)
                        };
                    }
                }
            }

            yield break;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #region GetRelativePath from http://stackoverflow.com/a/340454
        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path or <c>toPath</c> if the paths are not related.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        private static string GetRelativePath(string fromPath, string toPath)
        {
            if (string.IsNullOrEmpty(fromPath))
            {
                throw new ArgumentNullException("fromPath");
            }

            if (string.IsNullOrEmpty(toPath))
            {
                throw new ArgumentNullException("toPath");
            }

            Uri fromUri = new Uri(fromPath);
            Uri toUri = new Uri(toPath);

            // path can't be made relative.
            if (fromUri.Scheme != toUri.Scheme)
            {
                return toPath;
            }

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }
        #endregion
    }
}
