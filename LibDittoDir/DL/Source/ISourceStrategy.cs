﻿using System.Collections.Generic;
using DittoDir.DL.Transfer;

namespace DittoDir.DL.Source
{
    /// <summary>
    /// Defines the layout of any SourceStrategy.
    /// </summary>
    internal interface ISourceStrategy : System.IDisposable
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        string Source
        {
            get;

            set;
        }

        /// <summary>
        /// Generates all MetaInformation to copy.
        /// </summary>
        /// <returns>An enumerator on the requests MetaInformation.</returns>
        IEnumerable<MetaInformation> GenerateItems();
    }
}
