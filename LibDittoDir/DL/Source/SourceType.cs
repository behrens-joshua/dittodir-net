﻿namespace DittoDir.DL.Source
{
    /// <summary>
    /// The strategies to interpret a given Source.
    /// </summary>
    public enum SourceType
    {
        /// <summary>
        /// Interprets the given Source as a directory.
        /// </summary>
        Directory
    }
}
