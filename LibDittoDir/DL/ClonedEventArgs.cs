﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DittoDir.DL
{
    public class ClonedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the source specific item.
        /// </summary>
        public string SourceItem
        {
            get;

            private set;
        }

        /// <summary>
        /// Gets or sets the destination specific item.
        /// </summary>
        public string DestinationItem
        {
            get;

            set;
        }

        
        /// <summary>
        /// Indicates, whether the item is a file or a directory.
        /// </summary>
        public bool IsFile
        {
            get;

            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClonedEventArgs"/> class.
        /// </summary>
        /// <param name="sourceItem">The source item.</param>
        /// <param name="destinationItem">The destination item.</param>
        /// <param name="isFile">if set to <c>true</c> [is file].</param>
        public ClonedEventArgs(string sourceItem, string destinationItem, bool isFile)
        {
            SourceItem = sourceItem;
            DestinationItem = destinationItem;
            IsFile = IsFile;
        }
    }
}
