﻿using System.Collections.Generic;
using DittoDir.DL.Transfer;

namespace DittoDir.DL.Destination
{
    /// <summary>
    /// Defines the layout of any DestinationStrategy.
    /// </summary>
    internal interface IDestinationStrategy : System.IDisposable
    {
        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        string Destination
        {
            get;

            set;
        }

        /// <summary>
        /// Processes and saves any given MetaInformation.
        /// </summary>
        /// <param name="metaInformation">The metaInformation to save in the destination.</param>
        void ProcessItem(MetaInformation metaInformation);
    }
}
