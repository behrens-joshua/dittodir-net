﻿namespace DittoDir.DL.Destination
{
    /// <summary>
    /// Helping class to simplify general interactions with any destination specific class.
    /// </summary>
    internal class DestinationHelper
    {
        /// <summary>
        /// Gets the strategy by the given type.
        /// </summary>
        /// <param name="destinationType">Type of the destination strategy.</param>
        /// <returns>
        /// The corresponding strategy to the given destinationType.
        /// </returns>
        /// <exception cref="DittoDir.DL.Exceptions.DestinationStrategyNotImplementedException"></exception>
        public static IDestinationStrategy GetStrategyByType(DestinationType destinationType)
        {
            switch (destinationType)
            {
                case DestinationType.Directory:
                    return new Strategies.DirectoryStrategy();
            }

            throw new Exceptions.DestinationStrategyNotImplementedException(destinationType);
        }
    }
}
