﻿namespace DittoDir.DL.Destination
{
    /// <summary>
    /// The strategies to interpret a given Destination.
    /// </summary>
    public enum DestinationType
    {
        /// <summary>
        /// Interprets the given Destination as a directory.
        /// </summary>
        Directory
    }
}
