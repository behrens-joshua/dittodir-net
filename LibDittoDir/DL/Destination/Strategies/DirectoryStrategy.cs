﻿using System;
using System.IO;

namespace DittoDir.DL.Destination.Strategies
{
    /// <summary>
    /// Interprets the given destination as directory. Corresponding strategy to DestinationType.Directory.
    /// </summary>
    /// <seealso cref="DittoDir.DL.Destination.IDestinationStrategy" />
    internal class DirectoryStrategy : IDestinationStrategy
    {
        /// <summary>
        /// Processes and saves any given MetaInformation.
        /// </summary>
        /// <param name="metaInformation">The metaInformation to save in the destination.</param>
        public void ProcessItem(Transfer.MetaInformation metaInformation)
        {
            if (Destination == null)
            {
                throw new ArgumentNullException("this.Destination");
            }

            if (metaInformation.TransferItem == null)
            {
                throw new ArgumentNullException("metaInformation.TransferItem");
            }

            string path = metaInformation.DestinationItem ?? Path.Combine(Destination, metaInformation.TransferItem);

            FileInfo fileInfo = metaInformation.IsFile ? new FileInfo(path) : null;
            DirectoryInfo dirInfo = fileInfo == null ? new DirectoryInfo(path) : fileInfo.Directory;

            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }

            if (fileInfo != null)
            {
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }

                fileInfo.Create().Dispose();
                fileInfo.Refresh();
                fileInfo.CreationTime = metaInformation.Created;
                fileInfo.LastWriteTime = metaInformation.LastModified;
            }
        }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        public string Destination
        {
            get;

            set;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }
    }
}
